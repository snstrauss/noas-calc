import React, { Component } from 'react';

import logo from './logo.svg';

import './App.css';

class Welcome extends Component {
  state = {
    response: '',
    post: '',
    responseToPost: '',
  };

  componentDidMount() {
    this.callApi()
      .then(res => this.setState({ response: res.express }))
      .catch(err => console.log(err));
  }

  callApi = async () => {
    const response = await fetch('/api/hello');
    const body = await response.json();

    if (response.status !== 200) throw Error(body.message);

    return body;
  };

  handleSubmit = async e => {
    e.preventDefault();
    const response = await fetch('/api/world', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ post: this.state.post }),
    });
    const body = await response.text();

    this.setState({ responseToPost: body });
  };

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p>
            Hello QTrace 
          </p>
          {/* <a
            className="App-link"
            href="http://localhost:3001/"
            target="_blank"
            rel="noopener noreferrer"
          >
            Log in to calculator
          </a> */}
        </header>
        <p>{this.state.response}</p>
        <form onSubmit={this.handleSubmit}>
          <p>
            <strong>Insert User Name:</strong>
          </p>
          <input
            type="text"
            value={this.state.post}
            onChange={e => this.setState({ post: e.target.value })}
          />
          <button type="App-link">
          <a
            className="App-link"
            href="http://localhost:3001/"
            target="_blank"
            rel="noopener noreferrer"
          >
            Log in to calculator 
          </a>
          
          </button>
          <button type="Validate">
        
          Validate
          </button>
        </form>
        <p>{this.state.responseToPost}</p>
      </div>
    );
  }
}

export default Welcome;
