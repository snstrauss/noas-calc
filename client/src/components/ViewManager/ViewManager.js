import React, { Component } from 'react';

// import Welcome from '../viewa/welcome';
import Welcome from '../../welcome_post';
import Calc from '../viewa/calc';

class ViewManager extends Component {

    state = {
        view: 'welcome'
    }

    changeView = (newView) => {
        
        debugger;
        
        this.setState({
            view: newView
        })
    }

    render(){
        
        debugger;
        
        return (
            <div>
                <button style={buttonStyle} onClick={this.changeView.bind(this, 'welcome')}>welcome</button>
                <button style={buttonStyle} onClick={this.changeView.bind(this, 'calc')}>calci</button>
                <div style={currentViewStyle}>
                {
                    this.state.view === 'welcome'
                    ?
                    <Welcome></Welcome>
                    :
                    <Calc></Calc>
                }
                </div>
            </div>
        )
    }
}

const currentViewStyle = {
    border: '5px solid red'
}

const buttonStyle = {
    width: '50px',
    margin: '10px'
}

export default ViewManager;